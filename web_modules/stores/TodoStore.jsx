'use strict'

import Immutable from 'immutable/dist/immutable'
import {EventEmitter} from 'events'
import findIndex from 'lodash/array/findIndex'
import uniqueId from 'lodash/utility/uniqueId'
import dispatcher from '../dispatcher/dispatcher'

// A type of Map that has the additional guarantee that the iteration order of entries
// will be the order in which they were set().
var todos = new Immutable.OrderedMap()
// Creates a new Class which produces Record instances. A record is similar to a JS object,
// but enforce a specific set of allowed string keys, and have default values.
var Todo = Immutable.Record({
    id: '',
    label: '',
    isDone: false,
    order: 0
})

function add(label){
    var todo = new Todo({
        id: uniqueId(),
        label: label,
        order: todos.size
    })
    todos = todos.set(todo.id, todo)
}

function done(todo){
    todos = todos.set(todo.id, todo.set('isDone', true))
}

function undone(todo){
    todos = todos.set(todo.id, todo.set('isDone', false))
}

// store
class TodoStore extends EventEmitter {

    getState(){
        return todos.toList().sortBy(t => t.order )
    }

    emitChange(){
        this.emit('change')
    }

    addChangeListener(cb){
        this.on('change', cb)
    }

    removeChangeListener(cb){
        this.removeListener('change', cb)
    }
}

// create singleton and expose it
let todoStore = new TodoStore()
export default todoStore

// register to dispatcher
dispatcher.register(payload=>{
    var action = payload.action
    switch (action.actionType){
        case 'add':
            add(payload.action.label)
            todoStore.emitChange()
            break
        case 'done':
            done(payload.action.todo)
            todoStore.emitChange()
            break
        case 'undone':
            undone(payload.action.todo)
            todoStore.emitChange()
            break
    }
    return true
})