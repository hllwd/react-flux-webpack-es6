'use strict'

import $ from 'jquery/dist/jquery'
import React from 'react/addons'
import List from './components/List'

$(()=>{
    React.render(
        <List />,
        $('body').get(0)
    )
})