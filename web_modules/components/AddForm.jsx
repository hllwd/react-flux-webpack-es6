'use strict'

import React from 'react/addons'
import TodoActions from '../actions/TodoActions'

var AddForm = React.createClass({

    mixins: [React.addons.LinkedStateMixin, React.addons.PureRenderMixin],

    getInitialState: function(){
        return {
            label: ''
        }
    },

    handleAddButton: function(){
        TodoActions.add(this.state.label)
    },

    render: function(){
        return (
            <div>
                <input type="text" id="label" valueLink={this.linkState('label')}/>
                <button onClick={this.handleAddButton.bind(this)}>OK</button>
            </div>
        )
    }

})

export default AddForm