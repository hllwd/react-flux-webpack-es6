'use strict'

import React from 'react/addons'
import TodoActions from '../actions/TodoActions'

var Item = React.createClass({

    mixins: [React.addons.PureRenderMixin],

    handleDone: function(){
        TodoActions.done(this.props.item)
    },

    handleUndone: function(){
        TodoActions.undone(this.props.item)
    },

    render: function(){
        let isDragging = this.props.isDragging
        let connectDragSource = this.props.connectDragSource
        let label = this.props.item.isDone
            ? <strike>{this.props.item.label}</strike>
            : <span>{this.props.item.label}</span>

        let done = this.props.item.isDone
            ? <button onClick={this.handleUndone}>undone</button>
            : <button onClick={this.handleDone}>done</button>

        return (
            <li style={{opacity: isDragging ? 0.5 : 1}}>
                <span>{label}</span>
                <span>{done}</span>
            </li>
        )
    }
})

export default Item