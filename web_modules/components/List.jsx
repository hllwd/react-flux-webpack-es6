'use strict'

import React from 'react/addons'
import Item from './Item'
import TodoStore from '../stores/TodoStore'

var List = React.createClass({

    mixins: [React.addons.PureRenderMixin],

    getInitialState: function () {
        return {
            items: TodoStore.getState(),
            addForm: ''
        }
    },

    componentDidMount: function(){
        TodoStore.addChangeListener(this.onItemsChange.bind(this))
    },

    componentWillUnmount: function(){
        TodoStore.removeChangeListener(this.onItemsChange.bind(this))
    },


    onItemsChange: function () {
        this.setState({
            items: TodoStore.getState(),
            addForm: ''
        })
    },

    handleDisplayAddForm: function () {
        // une arrow fonction est bindée avec le this dans lequel elle est créée !!!!
        // http://javascriptplayground.com/blog/2014/04/real-life-es6-arrow-fn/
        require.ensure(['./AddForm'], (require)=> {
            var AddForm = require('./AddForm')
            this.setState({
                addForm: <AddForm/>
            })
        })
    },

    render: function () {
        return (
                <div>
                    <h1>Ma todo liste</h1>
                    <ul>
                        <li onClick={this.handleDisplayAddForm.bind(this)}>+</li>
                        {this.state.items.map((i, k)=> <Item item={i} key={k}/>)}
                    </ul>
                {this.state.addForm}
                </div>
        )
    }
})

export default List