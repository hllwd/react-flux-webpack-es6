'use strict'

import {Dispatcher} from 'flux'

class AppDispatcher extends Dispatcher {
    handleAction(action){
        this.dispatch({
            source: 'VIEW_ACTION',
            action: action
        })
    }
}

var dispatcher = new AppDispatcher()

// the dipatcher class is reachable only via a singleton isntance
export default dispatcher