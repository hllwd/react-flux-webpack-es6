import dispatcher from '../dispatcher/dispatcher'

var TodoActions = {
    add: function(label){
        dispatcher.handleAction({
            actionType: 'add',
            label: label
        })
    },
    done: function(todo){
        dispatcher.handleAction({
            actionType: 'done',
            todo: todo
        })
    },
    undone: function(todo){
        dispatcher.handleAction({
            actionType: 'undone',
            todo: todo
        })
    },
    destroy: function(todo){
        dispatcher.handleAction({
            actionType: 'destroy',
            todo: todo
        })
    }
}

export default TodoActions